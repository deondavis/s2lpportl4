
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "S2LP_Config.h"
#include "S2LP_Configuration_Common.h"
#include "S2LP_SDK_Util.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
/**
* @brief Radio structure fitting
*/
SRadioInit xRadioInit = {
  BASE_FREQUENCY,
  MODULATION_SELECT,
  DATARATE,
  FREQ_DEVIATION,
  BANDWIDTH
};
/**
* @brief Packet Basic structure fitting
*/
PktBasicInit xBasicInit={
  PREAMBLE_LENGTH,
  SYNC_LENGTH,
  SYNC_WORD,
  VARIABLE_LENGTH,
  EXTENDED_LENGTH_FIELD,
  CRC_MODE,
  EN_ADDRESS,
  EN_FEC,
  EN_WHITENING
};
/**
 * @brief GPIO IRQ structure fitting
 */
SGpioInit xGpioIRQ={
  S2LP_GPIO_3,
  S2LP_GPIO_MODE_DIGITAL_OUTPUT_LP,
  S2LP_GPIO_DIG_OUT_IRQ
};

/**
* @brief IRQ status struct declaration
*/
S2LPIrqs xIrqStatus;
/**
 * @brief Rx buffer declaration: how to store the received data
 */
uint8_t vectcRxBuff[128], cRxData;
uint8_t tmpReg = 0x00;
void SpiritBaseConfiguration(void);
/**
* @brief Preemption priority IRQ
*/
#define IRQ_PREEMPTION_PRIORITY         0x03
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
/**
 * @brief  This function handles External interrupt request. In this application it is used
 *         to manage the S2LP IRQ configured to be notified on the S2LP GPIO_3.
 * @param  None
 * @retval None
 */
static uint16_t M2S_GPIO_PIN_IRQ = GPIO3_Pin;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(GPIO_Pin==M2S_GPIO_PIN_IRQ)
  { 
    /* Get the IRQ status */
    S2LPGpioIrqGetStatus(&xIrqStatus);
    
    /* Check the S2LP RX_DATA_DISC IRQ flag */
    if(xIrqStatus.IRQ_RX_DATA_DISC) {
      /* toggle LED1 */
     // HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
      
//#ifdef USE_VCOM
//      printf("DATA DISCARDED\n\r");
//#endif

      /* RX command - to ensure the device will be ready for the next reception */
      S2LPCmdStrobeRx();
    }
    
    /* Check the S2LP RX_DATA_READY IRQ flag */
    if(xIrqStatus.IRQ_RX_DATA_READY) {
      /* Get the RX FIFO size */
      cRxData = S2LPFifoReadNumberBytesRxFifo();
      
      /* Read the RX FIFO */
      S2LPSpiReadFifo(cRxData, vectcRxBuff);
      
      /* Flush the RX FIFO */
      S2LPCmdStrobeFlushRxFifo();      
      
      /*  A simple way to check if the received data sequence is correct (in this case LED5 will toggle) */
      {
        SBool xCorrect=S_TRUE;
        
        for(uint8_t i=0 ; i<cRxData ; i++)
          if(vectcRxBuff[i] != i+1)
            xCorrect=S_FALSE;
        
        if(xCorrect) {
          /* toggle LED2 */
          HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
//#ifdef USE_VCOM
//          printf("DATA CORRECT, RSSI: %d dBm\r\n",S2LPRadioGetRssidBm());
//#endif
        }
      }      
      /* RX command - to ensure the device will be ready for the next reception */
      S2LPCmdStrobeRx();
      
//#ifdef USE_VCOM
//      /* print the received data */
//      printf("B data received: [");
//      for(uint8_t i=0 ; i<cRxData ; i++)
//        printf("%d ", vectcRxBuff[i]);
//      printf("]\r\n");
//#endif
    }    
    
  }
  
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
  
  /* S2LP ON */
  S2LPEnterShutdown();
  S2LPExitShutdown();
  
  /* EEPROM SPI if init + data retrieving */
//  S2LPManagementIdentificationRFBoard();
  
  /* if the board has eeprom, we can compensate the offset calling S2LPManagementGetOffset
  (if eeprom is not present this fcn will return 0) */
//  xRadioInit.lFrequencyBase = xRadioInit.lFrequencyBase + S2LPManagementGetOffset();
  
  /* if needed this will set the range extender pins */
//  S2LPManagementRangeExtInit();
  
  /* if needed this will set the EXT_REF bit of the S2-LP */
//  S2LPManagementTcxoInit();
  
  /* S2LP IRQ config */
  S2LPGpioInit(&xGpioIRQ);  
  
  /* S2LP Radio config */
  S2LPRadioInit(&xRadioInit);
  
   /* S2LP Packet config */
  S2LPPktBasicInit(&xBasicInit);
  
  SpiritBaseConfiguration();
  
//  for(uint8_t count = 0x2A;count<=0x6F;count++)
//  {
//    S2LPSpiReadRegisters(count, 1, &tmpReg);
//  }
  
   /* S2LP IRQs enable */
  S2LPGpioIrqDeInit(&xIrqStatus);
  S2LPGpioIrqConfig(RX_DATA_DISC,S_ENABLE);
  S2LPGpioIrqConfig(RX_DATA_READY,S_ENABLE);

  /* payload length config */
  //S2LPPktBasicSetPayloadLength(7);
 
  /* RX timeout config */
  S2LPTimerSetRxTimerUs(100000);
  //SET_INFINITE_RX_TIMEOUT();
  
  /* RX command */
  S2LPCmdStrobeRx();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
//uint8_t test_buff;
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 64;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void SpiritBaseConfiguration(void)
{
  uint8_t tmp;
  tmp = 0x00; /* reg. PCKTCTRL4 (0x2D) */
  S2LPSpiWriteRegisters(0x2D, 1, &tmp);
  tmp = 0x00; /* reg. PCKTCTRL3 (0x2E) */
  S2LPSpiWriteRegisters(0x2E, 1, &tmp);
  tmp = 0xE0; /* reg. RSSI_FLT (0x17) */
  S2LPSpiWriteRegisters(0x17, 1, &tmp);
  tmp = 0x10; /* reg. RSSI_TH (0x18) */
  S2LPSpiWriteRegisters(0x18, 1, &tmp);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
